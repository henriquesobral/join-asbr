<?php

namespace App;

use App\AppModel;
use Carbon\Carbon;
use Illuminate\Container\Container;
use Log;

class Lead extends AppModel
{

  protected static function boot() {
      parent::boot();

      static::creating(function($lead) {
        if (!$lead->isValid()) return false;

        $lead->calculateScore();
        return true;
      });

      static::created(function($lead) {
        // $lead->sendToApi();
        return true;
      });

  }

  protected static $rules = [
    'name' => ['required', 'regex:/^((\b\p{L}{2,255}\b)\s*){2,}$/'],
    'email' => ['required', 'email'],
    'phone' => ['required'],
    'born_at' => ['required', 'date_format:d/m/Y'],
    'unit_id' => ['required']
  ];

  /**
   * Default values for attributes
   * @var  array an array with attribute as key and default as value
   */
  protected $attributes = [
    'score' => 10
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'email', 'born_at', 'phone', 'unit_id'];

  public function setBornAtAttribute($value)
  {
      $this->attributes['born_at'] = Carbon::createFromFormat('d/m/Y', $value);
  }

  public function ageScoreModifier() {
    $dateForAgeCalc = Carbon::createFromDate(2016, 6, 1);
    $interval = $this->born_at->diffInYears($dateForAgeCalc);
    switch (true) {
      case ($interval >= 18 && $interval < 40):
        return 0;
        break;
      case ($interval >= 40 && $interval < 100):
        return -3;
        break;
      default:
        return -5;
        break;
    }
  }

  protected function calculateScore() {
    Log::info($this->unit->unitScoreModifier());
    Log::info($this->ageScoreModifier());
    $this->score = $this->score + $this->unit->unitScoreModifier() + $this->ageScoreModifier();
  }

  public function unit()
  {
      return $this->belongsTo('App\Unit');
  }

  protected function sendToApi() {
    $client = Container::getInstance()->offsetGet('guzzle');
    $ret = $client->post('http://api.actualsales.com.br/join-asbr/ti/lead', [
      'form_params' => [
        'nome'            => $this->name,
        'email'           => $this->email,
        'telefone'        => $this->phone,
        'regiao'          => $this->unit->region->name,         #(Elemento do conjunto ["Norte", "Nordeste", "Sul", "Sudeste", "Centro-Oeste"])
        'unidade'         => $this->unit->name,                 #(Elemento do conjunto ["Porto Alegre", "Curitiba", "São Paulo", "Rio de Janeiro", "Belo Horizonte", "Brasília", "Salvador", "Recife", "INDISPONÍVEL"])
        'data_nascimento' => $this->born_at->format('Y-m-d'),   #(data no formato YYYY-mm-dd)
        'score'           => $this->score,                      #(int de 0 a 10)
        'token'           => 'a85846a2a6506569a80030f2cad0f282' # (String)
      ]
    ]);
    Log::info(var_export($ret, true));
  }
}
