<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

  public function units()
  {
      return $this->hasMany('App\Unit');
  }

}
