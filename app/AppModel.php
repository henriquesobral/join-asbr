<?php

namespace App;

// use Validator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;

class AppModel extends Model
{
    protected static $rules = array();

    protected $errors;

    public function isValid()
    {
        // make a new validator object
        $v = Validator::make($this->getAttributes(), AppModel::$rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public static function rules() {
        $class = get_called_class();
        return $class::$rules;
    }
}
