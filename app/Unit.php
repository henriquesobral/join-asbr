<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['score_modifier', 'region_id', 'created_at', 'updated_at'];

    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function unitScoreModifier() {
      return $this->region->score_modifier + $this->score_modifier;
    }
}
