<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Region;
use App\Lead;

class LeadsController extends Controller
{

  /**
   * Responds to requests to GET /users
   */
  public function index()
  {
    $regions = Region::with('units')->get();
    return view('leads.index')->with('regions', $regions);
  }

  /**
   * Store a new lead.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    $messages = [
      'regex' => 'É necessário digitar o nome completo.',
      'email' => 'O email está incorreto',
      'required' => 'É de preenchimento obrigatório',
      'date_format' => 'Data inválida'
    ];
    $this->validate($request, Lead::rules(), $messages);
    $leadParams = $request->all();
    $lead = new Lead($leadParams);
    if ($lead->save()) {
        return response()->json(['status' => 'success']);
    } else {
        $regions = Region::with('units')->get();
        return view('products.create')->with('lead', $lead)->with('regions', $regions);
    }


    //
  }
}
