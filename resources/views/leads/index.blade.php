@extends('layouts.default')
@section('content')
  {!! BootForm::open()->action( route('leads.store') ); !!}
    <div class="row">
      <div class="col-lg-6" id="form-container">
        <div id="step_1" class="form-step">
          <div class="panel panel-info">
            <div class="panel-heading">
              <div class="panel-title">
                Preencha seus dados para receber contato
              </div>
            </div>
            <div class="panel-body">
              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Nome Completo</label>
                      <input class="form-control" type="text" name="name">
                      <span class="help-block"></span>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Data de Nascimento</label>
                      <input class="form-control" type="text" name="born_at" data-mask="00/00/0000">
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Email</label>
                      <input class="form-control" type="text" name="email">
                      <span class="help-block"></span>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Telefone</label>
                      <input class="form-control" type="text" name="phone">
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>

                <div>
                  <a class="btn btn-lg btn-info next-step">Próximo Passo</a>
                </div>
              </fieldset>
            </div>
          </div>
        </div>

        <div id="step_2" class="form-step" style="display:none">
          <div class="panel panel-info">
            <div class="panel-heading">
              <div class="panel-title">
                Preencha seus dados para receber contato
              </div>
            </div>
            <div class="panel-body">
              <fieldset>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Região</label>
                      <select class="form-control" id="region">
                        <option value="">Selecione a sua região</option>
                        @foreach ($regions as $region)
                          <option value="{{$region->id}}" data-units="{{$region->units->toJson()}}">{{$region->name}}</option>
                        @endforeach
                      </select>
                      <span class="help-block"></span>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="control-label">Unidade</label>
                      <select class="form-control" name="unit_id" id="units">
                        <option value="">Selecione a unidade mais próxima</option>
                      </select>
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>

                <div>
                  <button type="submit" class="btn btn-lg btn-info next-step">Enviar</button>
                </div>
              </fieldset>
            </div>
          </div>
        </div>

        <div id="step_sucesso" class="form-step" style="display:none">
          <div class="panel panel-info">
            <div class="panel-heading">
              <div class="panel-title">
                Obrigado pelo cadastro!
              </div>
            </div>
            <div class="panel-body">
                Em breve você receberá uma ligação com mais informações!
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <h1>Chamada interessante para o produto</h1>
        <h2>Mais uma informação relevante</h2>
      </div>
    </div>
  {!! BootForm::close() !!}
  <script>
    $(function () {
      $.applyDataMask();

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      function fillErrorInFields(response) {
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        for (fieldName in response) {
          $field = $('[name='+fieldName+']');
          $field.parents('.form-group').addClass('has-error');
          $field.siblings('.help-block').text(response[fieldName].join(', '));
        }
      }

      $('.next-step').click(function (event) {
        $this = $(this);
        $ret = $.post($('form').attr('action'), $('form').serialize());


        if ($(this).parents('.form-step').attr('id') === 'step_1') {
          $ret.fail(function (xhr){
            response = xhr.responseJSON;
            if (Object.keys(response).length > 1 ||
                Object.keys(response).indexOf('unit_id') < 0) {
              fillErrorInFields(response);
            } else {
              $this.parents('.form-step').hide().next().show();
            }
          });
        } else {
          $ret.fail(function (xhr){
            response = xhr.responseJSON;
            if (Object.keys(response).length === 1 &&
                Object.keys(response).indexOf('unit_id') >= 0) {
              fillErrorInFields(response);
            }
          }).done(function (){
            $this.parents('.form-step').hide().next().show();
          });
        }
        event.preventDefault();
      });

      $('#region').on('change', function(event) {
        var units = $('option:selected', this).data('units');
        $default = $('<option value="">Selecione a unidade mais próxima</option>');
        $('#units').empty().append($default);
        if (units) {
          for (unit of units) {
            $('#units').append('<option value="'+unit.id+'">'+unit.name+'</option>');
          }
        }
      });

      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };

      $('[name=phone]').mask(SPMaskBehavior, spOptions);
    });
  </script>
@stop
