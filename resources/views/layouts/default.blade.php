<!doctype html>
<html>
  <head>
      @include('includes.head')
  </head>
  <body>
    <div class="container">

      <div class="row" style="margin:30px 0">
        <div class="col-lg-3">
          <img src="img/logo.png" class="img-thumbnail">
        </div>
        <div class="col-lg-9">
          <h3>Nome do Produto</h3>
        </div>
      </div>

      @yield('content')

    </div>
  </body>
</html>
