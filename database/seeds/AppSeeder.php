<?php

use Illuminate\Database\Seeder;
use App\Region;
use App\Unit;

class AppSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // clear our database
    DB::table('leads')->delete();
    DB::table('units')->delete();
    DB::table('regions')->delete();

    // Regions
    $southRegion = Region::create(array(
      'name'           => 'Sul',
      'score_modifier' => -2
    ));

    $southEastRegion = Region::create(array(
      'name'           => 'Sudeste',
      'score_modifier' => -1
    ));

    $midWestRegion = Region::create(array(
      'name'           => 'Centro-Oeste',
      'score_modifier' => -3
    ));

    $northEastRegion = Region::create(array(
      'name'           => 'Nordeste',
      'score_modifier' => -4
    ));

    $northRegion = Region::create(array(
      'name'           => 'Norte',
      'score_modifier' => -5
    ));

    // Units
    // South Region
    Unit::create(array(
      'name'           => 'Porto Alegre',
      'region_id'      => $southRegion->id,
      'score_modifier' => 0
    ));

    Unit::create(array(
      'name'           => 'Curitiba',
      'region_id'      => $southRegion->id,
      'score_modifier' => 0
    ));

    // SouthEast Region
    Unit::create(array(
      'name'           => 'São Paulo',
      'region_id'      => $southEastRegion->id,
      'score_modifier' => 1
    ));

    Unit::create(array(
      'name'           => 'Rio de Janeiro',
      'region_id'      => $southEastRegion->id,
      'score_modifier' => 0
    ));

    Unit::create(array(
      'name'           => 'Belo Horizonte',
      'region_id'      => $southEastRegion->id,
      'score_modifier' => 0
    ));

    // MidWest Region
    Unit::create(array(
      'name'           => 'Brasilia',
      'region_id'      => $midWestRegion->id,
      'score_modifier' => 0
    ));

    // NorthEast Region
    Unit::create(array(
      'name'           => 'Salvador',
      'region_id'      => $northEastRegion->id,
      'score_modifier' => 0
    ));

    Unit::create(array(
      'name'           => 'Recife',
      'region_id'      => $northEastRegion->id,
      'score_modifier' => 0
    ));

    // North Region
    Unit::create(array(
      'name'           => 'INDISPONÍVEL',
      'region_id'      => $northRegion->id,
      'score_modifier' => 0
    ));

  }
}
